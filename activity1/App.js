import { useState }  from 'react';
import {StyleSheet, Button , View, Alert, TextInput} from 'react-native';

export default function App() {
const [input, setInput] = useState('');
{
  return (
    <View style={styles.container}>
      <TextInput 
        input Id = "text1"
        style={styles.input}  
        onChangeText ={(value) => setInput(value)}
        placeholder={'Enter text here'}
        placeholderTextColor = 'white'
        />
    
       <Button
        title="Submit"
        color ="black"
        
        onPress={() => alert ("You just typed:  "+ input)}
       />
       
    </View>
  );
  }
}
const styles = StyleSheet.create({
  input: {
    width: 200,
    borderColor: "#888",
    borderWidth: 4,
    borderRadius: 20,
    padding: 7,
    margin: 15,
  },
  container: {
    flex: 1,
    backgroundColor: '#e8dcdc',
    alignItems: 'center',
    justifyContent: 'center',
  },
});