import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button,ImageBackground } from 'react-native';
import Home from './screens/Home';
import I from './screens/I';
import II from './screens/II';
import III from './screens/III';
import IV from './screens/IV';



import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App(){
  return (
        <NavigationContainer>
         
          
          <Stack.Navigator>
            <Stack.Screen name ="Home" component={Home}/>
            <Stack.Screen name ="I" component={I}/>
            <Stack.Screen name ="II" component={II}/>
            <Stack.Screen name ="III" component={III}/>
            <Stack.Screen name ="IV" component={IV}/>
          </Stack.Navigator>
        </NavigationContainer>
        
  );
  
 
}


