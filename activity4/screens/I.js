import React from 'react'
import {View, Text ,Button,ImageBackground, StyleSheet} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import img01 from '../assets/img01.png'



const I = () =>{
    const navigation = useNavigation();
    return(
        <ImageBackground source={img01} resizeMode='cover'
        style={styles.imgbackground}>
         
        
            <View style={{
    
            position: 'absolute', 
            top: 0, left: 0, 
            right: 0, bottom: 0, 
            justifyContent: 'center', 
            alignItems: 'center'}}>
         
            
         <Text
            
            style={styles.black}></Text>
            <Text style={{borderWidth:1,position:'absolute',top:110,alignSelf:'center'}}>
            There are 3 Major Reasons Why I chose this course</Text>
           
            </View>
            
        <View style={{flex:1}}>
        <View style={{borderWidth:1,position:'absolute',bottom:20,alignSelf:'center'}}/>
             
            </View>
            
        <View style={{flex:1}}>
        <View style={{borderWidth:1,position:'absolute',bottom:20,alignSelf:'center'}}>
            
           <Button
             title="Continue"
             color="black"
             accessibilityLabel="Press"
            onPress={() =>navigation.navigate('II')}/>
         </View>
            </View>

           
            </ImageBackground>
    )

}

    const styles = StyleSheet.create({
        imgbackground: {
            flex:1,
            alignItems:'center', 
            justifyContent:'center',        
        },
        press: {
            flex:1,
            borderRadius:100,
            backgroundColor:'pink',
        },
        black: {
    
            color: 'black'
        },

       
        
    });

    

export default I;