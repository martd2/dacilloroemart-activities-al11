import React, { Component } from 'react';
import { Text, View, ImageBackground, StyleSheet, TextInput, TouchableOpacity, Image, Button} from 'react-native'

export default class App extends Component {

  componentDidMount() {
  }
  state = {
    height: 0,
      weight: 0,
        ResultInNumber: 0,
          ResultInText: '',
  }

  HandleCalculation = () => {

    let value = (this.state.weight) / (this.state.height)/ (this.state.height) * 10000;
    this.setState({
      ResultInNumber: value.toFixed(),
    });

    if (value < 18.5) {
      this.setState({ ResultInText: 'UnderWeight' })
      } else if (value >= 18.5 && value < 25) {
        this.setState({ ResultInText: 'Normal Weight' })
        } else if (value >= 25 && value < 30) {
          this.setState({ ResultInText: 'OverWeight' })
          } else if (value >= 30 && value < 34) {
            this.setState({ ResultInText: 'Obese' })
            } else if (value >= 34 && value < 39) {
              this.setState({ ResultInText: 'Severly Obese' })}     
  }
  render() {
    return (
      <ImageBackground source={require('./assets/g1.jpg')} style={{ width: '100%', height: '100%' }}>
        <View style={styles.container}>
          <Image
            style={{ width: 150, height: 80, borderRadius: 200, alignSelf: 'center', marginTop: 60 }}
            source={require('./assets/th.jpg')}
          />
          <View style={styles.intro}>
            <TextInput 
            placeholderTextColor='white'
            placeholder="Height(cm)" keyboardType="numeric" style={styles.input} onChangeText={height => this.setState({ height })} />
            <TextInput 
            placeholderTextColor='white'
            placeholder="weight(kg)" keyboardType="numeric" style={styles.input} onChangeText={weight => this.setState({ weight })} />
          </View>

          <TouchableOpacity onPress={this.HandleCalculation}>
            <Text style={styles.buttonText}>Calculate BMI</Text>
          </TouchableOpacity>
          <Text style={styles.result}>{this.state.ResultInNumber}</Text>
          <Text style={[styles.result, { fontSize: 30 }]}>{this.state.ResultInText}</Text>

          

        </View >
      </ImageBackground >

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  intro: {
    flexDirection: 'row',
  },
  input: {
    height: 100,
    textAlign: 'center',
    width: '51.5%',
    fontSize: 25,
    marginTop: 24,
    color: '#fff',
  },
  button: {
    backgroundColor: '#fff',
  },
  buttonText: {
    alignSelf: 'center',
    padding: 30,
    fontSize: 25,
    color: '#fff',
    fontWeight: 'bold',
  },
  result: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 55,
    padding: 15,
  },
});