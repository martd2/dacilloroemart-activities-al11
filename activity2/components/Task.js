import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Button } from "react-native";

const Task = (props) => {
  return (
    <View style={styles.item}>
      <View style={styles.itemLeft}>
        <View style={styles.square}></View>
        <Text style={styles.itemText}>{props.text}</Text>
      </View>
      <View style={styles.check}><Text>✓</Text></View>
    </View>
    
  );
};

const Todolist = () => {
  const navigation = useNavigation();
  return(
<View style={{flex:1,flexDirection: 'column', padding: 20}}>
        <Text style={{textAlignVertical: "center",textAlign: "center"}}>Completed Task Go Here</Text>
        <Button
        title='TodoApp'
        color='black'
        onPress={() =>navigation.navigate('Home')}>
        </Button>
        </View>
    
)}

const styles = StyleSheet.create({
  item: {
    backgroundColor: "#FFF",
    padding: 15,
    borderRadius: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  itemLeft: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
  },
  square: {
    width: 24,
    height: 24,
    backgroundColor: "#FFFFFF",
    opacity: 0.4,
    borderRadius: 5,
  },
  itemText: {
    maxWidth: "80%",
  },
  check: {
    width: 25,
    height: 25,
    borderColor: "#000000",
    backgroundColor: "#fff",
    alignItems: "center",
    borderWidth: 2,
    borderRadius: 5,
  },
});

export default Task;