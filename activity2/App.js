import React, { useState } from "react";
import Todolist from "./screens/Todolist";
import Home from "./screens/Home";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name=" " component={Home}/>
        <Stack.Screen name="Todolist" component={Todolist}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
