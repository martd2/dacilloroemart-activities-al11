import React from 'react'
import { View, Text, TouchableOpacity, Button, StyleSheet} from 'react-native'
import { useNavigation } from '@react-navigation/native'


const Home = () => {
    const navigation = useNavigation();
    return(
      <View style={[{ width:"100%", top: 250, backgroundColor:"lightgrey" }]}>
            <Text style={{textAlignVertical: "center",textAlign: "center"}}>COMPLETED TASK GO HERE</Text>
            <Button
            
            title='To-do-App'
            color='black'
            onPress={() =>navigation.navigate('Todolist')}>
            </Button>
            
        </View>
        
    )
}


    export default Home;
    
    